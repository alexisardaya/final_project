FROM ubuntu:latest


RUN apt-get update \
  && apt-get install -y python3-pip python3-dev \
  && cd /usr/local/bin \
  && ln -s /usr/bin/python3 python \
  && pip3 install --upgrade pip

WORKDIR /app

COPY EVO/requirements.txt /app/EVO/requirements.txt

RUN pip3 install -r /app/EVO/requirements.txt

COPY EVO  /app
COPY MESA /app

RUN pip3 install --editable .
EXPOSE 5000
ENTRYPOINT [ "python3" ]

CMD [ "main.py" ]
