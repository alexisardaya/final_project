import logging
import pytz
from datetime import datetime
from flask import Flask, jsonify, abort

from actor_counter import RunCounter
from config_reader import CamachoConfiguration
from models.clock_in_out import ClockInOut
from models.user import User

EXCHANGE = 'threading_example'
QUEUE = 'user.post'
ROUTING_KEY = 'test'
MESSAGE_COUNT = 100
app = Flask(__name__)
TIME_ZONE_BO = 'America/La_Paz'


@app.after_request
def after_request(response):
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Credentials', '*')
    response.headers.add('Access-Control-Allow-Headers',
                         "Content-Type, Accept, X-Requested-With, remember-me, Authorization, x-auth-token")
    response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
    return response


@app.route("/")
def index():
    return "Welcome to CAMACHO!"


def __get_datetime():
    datetime_bo = datetime.now(pytz.timezone(TIME_ZONE_BO))
    return datetime_bo.strftime('%d/%m/%Y %H:%M:%S')


@app.route('/api/user/clock-in-out/<user_code>', methods=['GET'])
def clock_in_out(user_code):
    ACTOR_COUNTER = RunCounter.run_actor_counter()
    if ACTOR_COUNTER:
        if not user_code:
            abort(400)
        user = User({'user_code': user_code})
        rec_user = user.validate_user(user.user_code)
        clock_in_out = ClockInOut({'user_id': rec_user[0]})
        records = clock_in_out.get_records_by_user_id()
        datetime_str = __get_datetime()
        app.logger.info('[%s] [User login] %s', datetime_str, user.user_id)
        return jsonify(records), 200
    return abort(500, 'Error de sistema')


if __name__ == '__main__':
    camacho_config = CamachoConfiguration()
    logging.basicConfig(filename=camacho_config.log_file_name, level=logging.INFO)
    app.run(host='0.0.0.0', port=9000)
