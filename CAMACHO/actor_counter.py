# -*- coding: utf-8 -*-
from thespian.actors import Actor
from thespian.actors import ActorSystem

ACTOR_COUNTER = False


class TicketCounter(Actor):
    def __init__(self, *args, **kwargs):
        self.count = 1
        super().__init__(*args, **kwargs)

    def receiveMessage(self, message, sender):
        if isinstance(message, str) and message == "can process?":
            msg = "Message: " + str(self.count)
            self.count += 1
            self.send(sender, msg)


class RunCounter:

    @staticmethod
    def run_actor_counter():
        global ACTOR_COUNTER
        if not ACTOR_COUNTER:
            counter = ActorSystem().createActor(TicketCounter)
            ActorSystem().ask(counter, "can process?")
            ACTOR_COUNTER = counter
        else:
            ActorSystem().ask(ACTOR_COUNTER, "can process?")
        return ACTOR_COUNTER
