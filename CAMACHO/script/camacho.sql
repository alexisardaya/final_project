CREATE TABLE public.user (
  id SERIAL PRIMARY KEY,
  user_id character varying,
  name character varying,
  password character varying
  );

INSERT INTO user(user_id, name, password) VALUES('JROCA101', 'Javier ROCA', '123456');

drop table user;

select * from user where id = 1;

select *
from user;
