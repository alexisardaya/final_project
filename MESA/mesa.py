import xml.etree.ElementTree as ET
from os import path

import click


# coding=utf-8


@click.command()
@click.argument('user_code', nargs=-1, required=True)
def log(user_code):
    source = click.open_file(path.join(path.abspath(path.dirname(__file__)), 'data.xml'))

    tree = ET.parse(source)
    data_root = tree.getroot()
    _validate_code(user_code)
    employee = find_employee_by_code(user_code, data_root)

    for data in employee:
        print(data.attrib['name'], ':', data.text)


def _validate_code(code):
    if not code:
        raise Exception('Please give me a code!')


def _validate_employee(employee):
    if len(employee) > 0:
        return employee[0]
    print('Unknown')
    return []


def find_employee_by_code(code, data_root):
    employee = list(filter(lambda x: code[0] == x.attrib['value'], data_root))
    return _validate_employee(employee)


if __name__ == '__main__':
    log()
