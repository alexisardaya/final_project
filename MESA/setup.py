# coding=utf-8
from setuptools import setup

setup(
    name="mesa",
    version='1.0',
    py_modules=['mesa'],
    install_requires=[
        'Click',
        'pytz',
    ],
    entry_points='''
        [console_scripts]
        mesa=mesa:log
    '''

)
