import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { SeguritoComponent } from './core/shell/segurito.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CoreModule } from './core/core.module';


@NgModule({
  declarations: [],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    CoreModule
  ],
  providers: [],
  bootstrap: [SeguritoComponent]
})
export class AppModule { }
