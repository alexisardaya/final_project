import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'segurito-root',
  template: '<router-outlet></router-outlet>',
  encapsulation: ViewEncapsulation.None
})
export class SeguritoComponent {
  title = 'SEGURITO';
}
