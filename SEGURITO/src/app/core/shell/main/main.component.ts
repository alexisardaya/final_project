import { ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';


@Component({
    selector: 'segurito-main-component',
    templateUrl: 'main-component.html',
    encapsulation: ViewEncapsulation.None
})
export class MainComponent implements OnInit {

    constructor() {
    }

    ngOnInit() {

    }
}
