import { NgModule } from '@angular/core';
import { CommonModule, HashLocationStrategy, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SeguritoComponent } from './shell/segurito.component';
import { ToolbarComponent } from './shell/toolbar/toolbar.component'
import { MainComponent } from './shell/main/main.component';
import { MatIconModule, MatToolbarModule, MatButton, MatButtonModule } from '@angular/material';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient, HttpClientModule } from '@angular/common/http';

const routes: Routes = [
    {
        path: '',
        redirectTo: 'user/login',
        pathMatch: 'full'
    },
    {
        path: 'user',
        component: MainComponent,
        loadChildren: './../user/user.module#UserModule'
    },
];

export function HttpLoaderFactory(http: HttpClient) {
    return new TranslateHttpLoader(http, 'assets/i18n/', '.json');
}

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forRoot(routes, { useHash: true }),
        MatIconModule,
        MatToolbarModule,
        MatButtonModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        }),
        HttpClientModule
    ],
    declarations: [
        SeguritoComponent,
        ToolbarComponent,
        MainComponent
    ],
    exports: [
        SeguritoComponent,
        ToolbarComponent,
        MainComponent
    ],
    providers: [
        PathLocationStrategy,
        { provide: LocationStrategy, useClass: HashLocationStrategy }
    ]
})
export class CoreModule {
}
