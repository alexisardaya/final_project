import { Component, OnInit, Input, EventEmitter, Output, ChangeDetectorRef, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';
import { ResUser } from '../../shared/entities/res_user';
import { UserService } from '../service/user.service';
import { SpinnerService } from 'src/app/shared/service/spinner.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TranslateService } from '@ngx-translate/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';

@Component({
    selector: 'user-form',
    templateUrl: 'user-form.component.html',
    styleUrls: ['user-form.component.css']
})
export class UserFormComponent implements OnInit {
    displayedColumns: string[] = ['type', 'date'];
    dataSource = new MatTableDataSource<ResUser>([]);
    @ViewChild(MatPaginator) paginator: MatPaginator;

    resUserEntity: ResUser = new ResUser();
    durationInSeconds = 5;
    resultsLength = 0;
    public minDate: Date = new Date("05/07/2017 2:00 AM");
    public maxDate: Date = new Date("05/27/2017 11:00 AM");
    public dateValue: Date = new Date("05/16/2017 5:00 AM");

    public resUserForm: FormGroup;
    public activeLang = 'es';

    constructor(private formBuilder: FormBuilder,
        private userService: UserService,
        public spinnerService: SpinnerService,
        private changeDetectorRefs: ChangeDetectorRef,
        private snackBar: MatSnackBar,
        private translate: TranslateService) {
        let browserLang = translate.getBrowserLang();
        translate.use(browserLang.match(/es|id/) ? browserLang : 'es');
    }

    ngOnInit() {
        this.initForm();
    }

    initForm() {
        this.resUserForm = this.formBuilder.group({
            userId: [{
                value: '',
            }, Validators.compose([Validators.required, Validators.minLength(7), Validators.maxLength(20)])],
        })
    }

    save() {
        this.createUserClockInOut();
    }

    search() {
        this.searchUserClockInOut();
    }

    createUserClockInOut() {
        this.spinnerService.emitChange(true);
        const UserInOutValueForm = this.resUserForm.value;
        const userInOut: ResUser = <ResUser>{
            user_code: UserInOutValueForm.userId,
        };
        this.userService.login(userInOut).subscribe(
            res => {
                if (res.status === 201) {
                    this.openSnackBar("Registro realizado correctamente", "OK")
                }
            },
            error => {
                if (error.status === 400) {
                    return this.openSnackBar("Error al realizar el registro", "CANCEL")
                }
                if (error.status === 404) {
                    return this.openSnackBar("Usuario no encontrado", "CANCEL")
                }
                return this.openSnackBar("Error en el sistema", "CANCEL")
            });
        this.clean({})
        this.spinnerService.emitChange(false);
    }

    searchUserClockInOut() {
        this.spinnerService.emitChange(true);
        const UserInOutValueForm = this.resUserForm.value;
        const userInOut: ResUser = <ResUser>{
            user_code: UserInOutValueForm.userId,
        };
        const data = this.userService.getDataByResId(userInOut).subscribe(
            res => {
                this.dataSource = res
                this.resultsLength = res.length;
                this.changeDetectorRefs.detectChanges();
                this.dataSource.paginator = this.paginator;
            },
            error => {
                if (error.status === 400) {
                    return this.openSnackBar("Error al realizar el registro", "CANCEL")
                }
                if (error.status === 404) {
                    return this.openSnackBar("Usuario no encontrado", "CANCEL")
                }
                return this.openSnackBar("Error en el sistema", "CANCEL")
            });

        this.clean({})
        this.spinnerService.emitChange(false);
    }

    clean(value: any = undefined) {
        this.resUserForm.reset(value)
        this.dataSource = value
    }

    openSnackBar(message, action) {
        this.snackBar.open(message, action, {
            duration: 3000
        });
    }

    onPaginateChange(event){
        this.dataSource.paginator = this.paginator;
    }
}
