import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { environment } from './../../../environments/environment';
import { map } from 'rxjs/operators'

@Injectable()
export class UserService {

    private urlCamcho = environment.backendCamachoUrl + '/user/';
    private urlEvo = environment.backendEvoUrl + '/user/';
    constructor(private http: Http) {
    }

    login(userAuth: any) {
        const headers = new Headers({
            'Content-Type': 'application/json',
        });
        return this.http.post(this.urlEvo + 'login', userAuth, { headers }).pipe(
            map((response) => response)
        )
    }

    getDataByResId(userAuth: any) {
        return this.http.get(this.urlCamcho + 'clock-in-out/' + userAuth.user_code).pipe(
            map((response) => response.json())
        )
    }
}
