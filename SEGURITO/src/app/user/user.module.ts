import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { UserFormComponent } from './user-form/user-form.component';
import { MatFormFieldModule, MatOptionModule, MatSelectModule, MatInputModule, MatCardModule, MatButtonModule, MatDividerModule, MatPaginator, MatPaginatorModule } from '@angular/material';
import { DateTimePickerModule } from '@syncfusion/ej2-angular-calendars';
import { ReactiveFormsModule } from '@angular/forms';
import { UserService } from './service/user.service';
import { HttpModule } from '@angular/http';
import { SpinnerService } from '../shared/service/spinner.service';
import { SpinnerComponent } from '../shared/spinner/spinner.component';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTableModule } from '@angular/material/table';
import { TranslateModule } from '@ngx-translate/core';
import { HttpClientModule } from '@angular/common/http';

const routes: Routes = [
    {
        path: '',
        children: [
            {
                path: 'login',
                component: UserFormComponent,
            }
        ]
    }

]

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        MatFormFieldModule,
        MatOptionModule,
        MatSelectModule,
        MatInputModule,
        MatCardModule,
        MatButtonModule,
        DateTimePickerModule,
        ReactiveFormsModule,
        HttpModule,
        MatSnackBarModule,
        MatButtonModule,
        MatTableModule,
        TranslateModule,
        HttpClientModule,
        MatDividerModule,
        MatPaginatorModule
    ],
    declarations: [
        UserFormComponent,
        SpinnerComponent
    ],
    exports: [
        UserFormComponent,
        SpinnerComponent
    ],
    providers: [
        UserService,
        SpinnerService
    ]
})
export class UserModule {
}
