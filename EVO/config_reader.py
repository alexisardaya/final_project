# -*- encoding: utf-8 -*-
import configparser
from os import path

CONFIG_FILE = "evo.conf"


class EvoConfiguration:
    def __init__(self):
        self.__configuration_parser = configparser.ConfigParser()
        absolute_file_path = path.join(path.abspath(path.dirname(__file__)), CONFIG_FILE)
        self.__configuration_parser.read(absolute_file_path)

    @property
    def postgres_host(self):
        return self.__configuration_parser.get(section='postgres', option='host')

    @property
    def postgres_user(self):
        return self.__configuration_parser.get(section='postgres', option='user')

    @property
    def postgres_password(self):
        return self.__configuration_parser.get(section='postgres', option='password')

    @property
    def postgres_port(self):
        return self.__configuration_parser.get(section='postgres', option='port')

    @property
    def postgres_database(self):
        return self.__configuration_parser.get(section='postgres', option='database')

    @property
    def log_file_name(self):
        return self.__configuration_parser.get(section='evo', option='log_file_name')

    @property
    def rabbit_url(self):
        return self.__configuration_parser.get(section='rabbit', option='url')

    @property
    def mesa_path(self):
        return self.__configuration_parser.get(section='mesa', option='path')
