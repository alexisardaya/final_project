# -*- encoding: utf-8 -*-
import json
import subprocess
from datetime import datetime

import pika

DEFAULT_SERVER_DATE_FORMAT = "%Y-%m-%d %H:%M:%S"
QUEUE = 'evo-chi'
TIME_ZONE_BO = 'America/La_Paz'


class User:
    def __init__(self, user):
        self._user_code = user.get("user_code", False)

    def sync(self, user_code):
        credential_params = pika.PlainCredentials("pconpuub", "Y8IjBDtuqg2BfSnGIkRXAb0bYjEgMbax")
        params = pika.ConnectionParameters(credentials=credential_params, virtual_host="pconpuub",
                                           host="lion.rmq.cloudamqp.com", heartbeat=0)

        connection = pika.BlockingConnection(params)
        channel = connection.channel()
        user = self.prepare_json_user(user_code)
        channel.exchange_declare(exchange=QUEUE, exchange_type='fanout')
        channel.basic_publish(exchange=QUEUE,
                              routing_key='',
                              properties=pika.BasicProperties(content_type='application/json'),
                              body=json.dumps(user),
                              )
        connection.close()

    def validate_user_from_mesa(self):
        command_success = "mesa {}".format(self._user_code)
        return subprocess.getoutput(command_success)

    def prepare_json_user(self, user_code):
        date = datetime.now()
        return {
            "user_code": user_code,
            "date": date.strftime(DEFAULT_SERVER_DATE_FORMAT),
        }

    @property
    def user_code(self):
        return self._user_code

    @user_code.setter
    def user_code(self, user_code):
        self._user_code = user_code

    def __repr__(self):
        return "Usuario:" % self.user_code
