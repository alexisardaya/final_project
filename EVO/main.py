import logging
from datetime import datetime

import pytz
from flask import Flask, jsonify, abort, request
from flask_cors import CORS

from actor_counter import RunCounter
from config_reader import EvoConfiguration
from models.user import User

EXCHANGE = 'threading_example'
ROUTING_KEY = 'test'
MESSAGE_COUNT = 100

app = Flask(__name__)
CORS(app, resources={r"*": {"origins": "*"}})

TIME_ZONE_BO = 'America/La_Paz'


@app.route("/")
def index():
    return "Welcome to EVO!"


def __get_datetime():
    datetime_bo = datetime.now(pytz.timezone(TIME_ZONE_BO))
    return datetime_bo.strftime('%d/%m/%Y %H:%M:%S')


@app.route('/api/user/login', methods=['POST'])
def loggin_user():
    if not request.json or not 'user_code' in request.json:
        abort(400)
    user = User(request.json)
    result = RunCounter().run_actor_counter(user)
    if result == 'Unknown':
        return abort(404, 'Usuario no encontrado')
    user.sync(user.user_code)
    datetime_str = __get_datetime()
    app.logger.info('[%s] [User login] %s', datetime_str, user.user_code)
    return jsonify({'user_code': user.user_code}), 201


if __name__ == '__main__':
    evo_config = EvoConfiguration()
    logging.basicConfig(filename=evo_config.log_file_name, level=logging.INFO)
    app.run(host='0.0.0.0', port=5000)
