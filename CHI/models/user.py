# -*- encoding: utf-8 -*-
import subprocess
from os import path

from database import Database


class User:
    def __init__(self, user):
        self._user_id = user.get('user_id', False)
        self._name = user.get('name', False)
        self._user_code = user.get("user_code", False)

    def create(self):
        create_user_query = """
        INSERT INTO res_user(user_id, name, user_code) VALUES( %(user_id)s, %(name)s, %(user_code)s)"""
        db = Database()
        ps_connection = db.session()
        ps_cursor = ps_connection.cursor()
        ps_cursor.execute(create_user_query, {
            "user_id": self._user_id,
            "name": self._name,
            "user_code": self._user_code,
        })
        ps_connection.commit()
        ps_cursor.close()
        return self.user_id

    def update(self, user_code):
        user_query = """update res_user set name= %(name)s, user_id= %(user_id)s where user_code =  %(user_code)s"""
        db = Database()
        ps_connection = db.session()
        ps_cursor = ps_connection.cursor()
        ps_cursor.execute(user_query, {
            "user_id": self._user_id,
            "name": self._name,
            "user_code": self._user_code,
        })
        ps_cursor.close()
        return user_code

    @staticmethod
    def browse(user_code):
        browse_user_query = """select * from res_user where user_code = %(user_code)s order by id desc limit 1;"""
        db = Database()
        ps_connection = db.session()
        ps_cursor = ps_connection.cursor()
        ps_cursor.execute(browse_user_query, {
            "user_code": user_code
        })
        user = ps_cursor.fetchone()
        ps_cursor.close()
        return user

    @staticmethod
    def validate_user(user_code):
        browse_user_query = """select * from res_user
         where user_code = %(user_code)s order by id desc limit 1;"""
        db = Database()
        ps_connection = db.session()
        ps_cursor = ps_connection.cursor()
        ps_cursor.execute(browse_user_query, {
            "user_code": user_code
        })
        user = ps_cursor.fetchone()
        ps_cursor.close()
        return user

    @property
    def user_id(self):
        return self._user_id

    @property
    def name(self):
        return self._name

    @property
    def user_code(self):
        return self._user_code

    @user_id.setter
    def id_user(self, user_id):
        self._user_id = user_id

    @name.setter
    def name(self, name):
        self._name = name

    @user_code.setter
    def user_code(self, user_code):
        self._user_code = user_code

    def __repr__(self):
        return "Usuario: %s" % self.user_code
