# -*- encoding: utf-8 -*-
import json
from datetime import datetime

import pika

from config_reader import ChiConfiguration
from database import Database

TIME_ZONE_BO = 'America/La_Paz'
DEFAULT_SERVER_DATE_FORMAT = "%Y-%m-%d %H:%M:%S"
QUEUE = 'segurito'


class ClockInOut:
    def __init__(self, clock_in_out):
        self._user_id = clock_in_out.get('user_id', False)
        self._type = clock_in_out.get('type', False)
        self._date = clock_in_out.get('date', False)

    def create_loggin_user(self, user_code):
        last_record = self.browse()
        self._type = self._get_type(last_record)
        self.create()
        self.sync(user_code)

    def create(self):
        create_clock_in_out_query = """
        INSERT INTO clock_in_out(user_id, type, date) VALUES( %(user_id)s, %(type)s, %(date)s)"""
        db = Database()
        ps_connection = db.session()
        ps_cursor = ps_connection.cursor()
        ps_cursor.execute(create_clock_in_out_query, {
            "user_id": self._user_id,
            "type": self._type,
            "date": self._date,
        })
        ps_connection.commit()
        ps_cursor.close()
        return self.user_id

    @staticmethod
    def _get_type(last_record):
        if not last_record:
            return 'clock-in'
        if last_record[2] == 'clock-in':
            return 'clock-out'
        else:
            return 'clock-in'

    def browse(self):
        browse_clock_in_out_query = """select * from clock_in_out 
        where user_id = %(user_id)s order by id desc limit 1;"""
        db = Database()
        ps_connection = db.session()
        ps_cursor = ps_connection.cursor()
        ps_cursor.execute(browse_clock_in_out_query, {
            "user_id": self._user_id
        })
        record_clock_in_out = ps_cursor.fetchone()
        ps_cursor.close()
        return record_clock_in_out

    def get_records_by_user_id(self):
        browse_clock_in_out_query = """select * from clock_in_out 
        where user_id = %(user_id)s order by id desc"""
        db = Database()
        ps_connection = db.session()
        ps_cursor = ps_connection.cursor()
        ps_cursor.execute(browse_clock_in_out_query, {
            "user_id": self._user_id
        })
        records_clock_in_out = ps_cursor.fetchall()
        ps_cursor.close()
        return [self.prepare_json_clock_in_out_query(row) for row in records_clock_in_out]

    def prepare_json_clock_in_out_query(self, row):
        return {
            "type": row[2],
            "date": self.string_to_millis_bo(str(row[3])),
        }

    def string_to_millis_bo(self, date_time):
        if date_time:
            return int(self.string_to_datetime(date_time).timestamp() * 1000)
        else:
            return 0

    @staticmethod
    def string_to_datetime(_str_datetime, _format=DEFAULT_SERVER_DATE_FORMAT):
        _datetime = datetime.strptime(_str_datetime, _format)
        return _datetime

    @property
    def user_id(self):
        return self._user_id

    @property
    def type(self):
        return self._type

    @property
    def date(self):
        return self._date

    @user_id.setter
    def id_user(self, user_id):
        self._user_id = user_id

    @type.setter
    def type(self, type):
        self._type = type

    @date.setter
    def date(self, date):
        self._date = date

    def __repr__(self):
        return "Usuario:" % self._user_id

    def sync(self, user_code):
        configuration = ChiConfiguration()
        url = configuration.rabbit_url
        params = pika.URLParameters(url)
        connection = pika.BlockingConnection(params)
        channel = connection.channel()
        user = self.prepare_json_user(user_code)
        channel.exchange_declare(exchange='segurito', exchange_type='fanout')
        channel.basic_publish(exchange='segurito',
                              routing_key='',
                              properties=pika.BasicProperties(content_type='application/json'),
                              body=json.dumps(user),
                              )
        connection.close()

    def prepare_json_user(self, user_code):
        return {
            "user-code": user_code,
            "entry-type": self._type,
            "entry-datetime": self._date
        }
