import os
from datetime import datetime

import pika
import pytz

from actor_counter import RunCounter
from config_reader import ChiConfiguration
from models import User, ClockInOut

TIME_ZONE_BO = 'America/La_Paz'
configuration = ChiConfiguration()

rabbit_url = configuration.rabbit_url
url = os.environ.get('CLOUDAMQP_URL', rabbit_url)
credential_params = pika.PlainCredentials("pconpuub", "Y8IjBDtuqg2BfSnGIkRXAb0bYjEgMbax")
params = pika.ConnectionParameters(credentials=credential_params, virtual_host="pconpuub", host="lion.rmq.cloudamqp.com", heartbeat=0)

connection = pika.BlockingConnection(params)
connection.process_data_events(time_limit=1)
channel = connection.channel()
channel.exchange_declare(exchange='evo-chi', exchange_type='fanout')
result = channel.queue_declare(queue='evo-chi')
queue_name = result.method.queue

channel.queue_bind(exchange='evo-chi', queue=queue_name)
channel.basic_qos(prefetch_count=1)


def __get_datetime():
    datetime_bo = datetime.now(pytz.timezone(TIME_ZONE_BO))
    return datetime_bo.strftime('%d/%m/%Y %H:%M:%S')


# channel.basic_publish(exchange='',
#                       routing_key='evo-chi',
#                       properties=pika.BasicProperties(content_type='application/json'),
#                       body=json.dumps({"user_code": "1234567", "date": "2019-05-07 22:02:03"}))


def callback(ch, method, properties, body):
    print(" [x] Received " + str(body))
    if body:
        body_dict = eval(body.decode("utf-8"))
        user = User(body_dict)
        rec_user = RunCounter().run_actor_counter(user)
        clock_in_out = ClockInOut({'user_id': rec_user[0], 'date': body_dict['date']})
        clock_in_out.create_loggin_user(rec_user[1])
        datetime_str = __get_datetime()
        print('[%s] [User login] %s', datetime_str, user.user_code)
        return


channel.basic_consume(queue='evo-chi',
                      on_message_callback=callback,
                      auto_ack=True)
print(' [*] Waiting for messages:')
channel.start_consuming()
connection.close()
